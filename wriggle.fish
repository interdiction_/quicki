#!/usr/local/bin/fish
function whale_b
echo '''
      ":"
    ___:____     |"\/"|
  ,        `.    \  /
  |  O        \___/  |
~^~^~^~^~^~^~^~^~^~^~^~^~bloop'''
end
function clean_it
command clear
fish_logo
end
#crtsh addition
function crtit
command curl -s 'https://crt.sh/?q='"$argv[1]" -H 'authority: crt.sh' -H 'sec-ch-ua: "Chromium";v="94", "Microsoft Edge";v="94", ";Not A Brand";v="99"' -H 'sec-ch-ua-mobile: ?0' -H 'sec-ch-ua-platform: "Windows"' -H 'upgrade-insecure-requests: 1' -H 'dnt: 1' -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36 Edg/94.0.992.31' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'sec-fetch-site: same-origin' -H 'sec-fetch-mode: navigate' -H 'sec-fetch-user: ?1' -H 'sec-fetch-dest: document' -H 'referer: https://crt.sh/' -H 'accept-language: en-US,en;q=0.9' -H 'sec-gpc: 1' --compressed |grep -Eo '([a-z0-9A-Z]\.)*[a-z0-9-]+\.([a-z0-9]{2,24})+(\.co\.([a-z0-9]{2,24})|\.([a-z0-9]{2,24}))*' |sort -u |grep "$argv[1]" >> dns.scrap
end
clean_it
if test -f blowhole.rb; and test -r blowhole.rb; command sleep 0.1; else; echo "fail: scraper"; exit 1; end
if command curl -s ipinfo.io |jq -r .region; command sleep 0.1; else; echo "fail: inet"; exit 1; end
if command curl --insecure --proxy 127.0.0.1:8080 curl -s https://google.com; command sleep 0.1; else; echo "fail: burp"; exit 1; end
if test -f dns.scrap; command rm dns.scrap; end && if test -f urls.txt; command rm urls.txt; end
if command pgrep openvpn; command sleep 0.1; else; echo "fail: vpn"; exit 1; end
if test "$argv[1]"; command sleep 0.1; else; echo "fail: domain arg"; exit 1; end
command clear && whale_b && echo "subdomain enum: $argv[1]"
command amass enum -d "$argv[1]" >> dns.scrap
crtit
command sort -u dns.scrap -o dns.scrap
if test dns.scrap; command sleep 0.1; else; echo "fail: no subdomains"; exit 1; end
echo "scraping"
command cat dns.scrap | while read -l foo; command timeout 180 ruby blowhole.rb --net https://$foo urls.txt; end
command cat dns.scrap | while read -l foz; command wayback_machine_downloader -l $foz |jq -r '.[].file_url' >> urls.txt &; end
wait
command sort -u urls.txt -o urls.txt
if test urls.txt; command sleep 0.1; else; echo "fail: no urls"; exit 1; end
command cat urls.txt | while read -l fox; command curl --connect-timeout 4 --insecure --proxy 127.0.0.1:8080 -s -o /dev/null $fox; end
if test -f dns.scrap; command rm dns.scrap; end && if test -f urls.txt; command rm urls.txt; end
clean_it
echo 'success: targets added to burp' && echo "domains:"
command amass db -names -d "$argv[1]"