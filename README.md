<div align="center">

## Quicki

Makes the start of your OSINT just that little bit simpler **_and safer_**

<img src="images/whale.gif" width="350">

</div>

<div align="center">

### :whale:
<img src="images/main.png">

</div>

## :tropical_fish: How does it help you?

- [x] checks vpn is connected _openvpn_
- [x] checks burp **OR** zap proxy is running _127.0.0.1:8080_
- [x] subdomain enumeration targets _added crt[.]sh scraper_
- [x] scrapes target
- [x] waybackmachine url collection target
- [x] sends all urls to burp
- [x] sweet ascii art

### :file_folder: TLDR; It Works on kali (debian)

If you do not use kali, use your own method to install `amass`. https://github.com/OWASP/Amass

## :whale2: Getting started

Uses fish shell because its more efficient than bash (plus has efficient syntax)

- [x] install the deps

```
sudo apt install -y fish
fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install laughedelic/fish_logo
sudo apt install -y ruby && sudo gem install spidr && sudo gem install wayback_machine_downloader
```
- [x] launch burpsuite/zap
- [x] connect to your "openvpn"

```
fish wriggle.fish domaininquestion.com
```
